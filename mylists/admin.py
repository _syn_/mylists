from django.contrib import admin
from .models import *

admin.site.register(Article)
admin.site.register(Liste)
admin.site.register(Stock)
admin.site.register(Enseigne)
# Register your models here.
