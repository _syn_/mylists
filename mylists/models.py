from django.db import models

# Create your models here.

class Article(models.Model):
    nom  = models.CharField(max_length=200)
    QUANTITE = models.CharField(max_length=200)
    PRIX_BASE = models.CharField(max_length=200)
    
class Liste(models.Model):
    #Id = models.CharField(primary_key=True,max_length=200)
    PRIX_TOTAL = models.CharField(max_length=200)
    ARTICLES = models.ForeignKey(Article, on_delete = models.CASCADE)

class Stock(models.Model):
    #Id = models.CharField(max_length=200)
    articles = models.ForeignKey(Article, on_delete  = models.CASCADE)

class Enseigne(models.Model):
    
    #Id = models.CharField(primary_key=True,max_length=200)
    MODIFICATEUR_PRIX = models.CharField(max_length=200)
    ARTICLE = models.ForeignKey(Article, on_delete  = models.CASCADE)
    STOCK = models.ForeignKey(Stock, on_delete  = models.CASCADE)
