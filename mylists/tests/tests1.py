from django.test import TestCase

# Create your tests here.
from ..models import Article




class ParticipantTestCase(TestCase):
    def setUp(self):
        Article.objects.create(nom="srs a2")
        
    def test_one_participant(self):
        result = Article.objects.all()
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].nom,'srs a2')
        