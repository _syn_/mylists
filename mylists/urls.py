# -*- coding: utf-8 -*-

from django.urls import path

from . import views

urlpatterns = [
    path('',views.index,name='index'),
    path('articles',views.get_all_articles,name ='get_all_articles'),
    path('articles/<str:nom>',views.create_article, name='create_article'),
    ]